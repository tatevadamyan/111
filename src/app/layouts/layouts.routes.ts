import { Routes, RouterModule } from '@angular/router';
import { LayoutsComponent } from './layouts.component';

const LAYOUT_ROUTES: Routes = [
    {
        path: "",
        component: LayoutsComponent,
        children:[
            {
                path: "login",
                loadChildren: "./login-layout/login-layout.module#LoginLayoutModule"
            }
        ]
    },
    {
        path: "**",
        redirectTo: "login"
    }
];
export const LayoutRoutes = RouterModule.forChild(LAYOUT_ROUTES);