import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutRoutes } from './layouts.routes';
import { LayoutsComponent } from './layouts.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
    imports: [
        LayoutRoutes,
        CommonModule,
        SharedModule
    ],
    declarations: [LayoutsComponent]
})
export class LayoutsModule { }