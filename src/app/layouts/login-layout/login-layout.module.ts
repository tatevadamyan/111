import { NgModule } from '@angular/core';
import { Routes, RouterModule } from "@angular/router";
import { LoginLayoutRoutes } from './login-layout.routes';
import { LoginLayoutComponent } from './login-layout.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
    declarations: [
        LoginLayoutComponent
    ],
    imports: [
        LoginLayoutRoutes,
        SharedModule
    ]
})
export class LoginLayoutModule { }