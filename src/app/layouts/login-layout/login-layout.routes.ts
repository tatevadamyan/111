import { Routes, RouterModule } from "@angular/router";
import { LoginLayoutComponent } from './login-layout.component';

const LOGIN_ROUTES: Routes = [
    {
        path: "",
        component: LoginLayoutComponent,
        children: [
            {
                path: "owner",
                loadChildren: "../../pages/owner-page/owner.module#OwnerModule"
            },
            {
                path: "doctor",
                loadChildren: "../../pages/doctor-page/doctor.module#DoctorModule"
            },
            {
                path: "patient",
                loadChildren: "../../pages/patient-page/patient.module#PatientModule"
            }
        ]
    }
];
export const LoginLayoutRoutes = RouterModule.forChild(LOGIN_ROUTES);