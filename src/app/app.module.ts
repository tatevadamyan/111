import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutes} from './app.routes';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { LoginLayoutComponent } from './layouts/login-layout/login-layout.component';
// import { OwnerPageComponent } from './pages/owner-page/owner-page.component';
import { DoctorPageComponent } from './pages/doctor-page/doctor-page.component';
import { PatientPageComponent } from './pages/patient-page/patient-page.component';

import { SharedModule } from './shared/shared.module';



@NgModule({
  declarations: [
    AppComponent,
    // LoginLayoutComponent,
    // OwnerPageComponent,
    // DoctorPageComponent,
    // PatientPageComponent
  ],

  imports: [
    BrowserModule,
    FormsModule,
    AppRoutes,
    BrowserAnimationsModule,
    SharedModule.forRoot()
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
