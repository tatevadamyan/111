import { NgModule } from '@angular/core';
import { Routes, RouterModule } from "@angular/router";
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module'
import { OwnerPageComponent } from './owner-page.component';

const OWNER_ROUTES: Routes = [{
    path: "",
    component: OwnerPageComponent
}];

@NgModule({
    declarations: [OwnerPageComponent],
    imports: [
        SharedModule,
        FormsModule,
        RouterModule.forChild(OWNER_ROUTES)
    ]
})
export class OwnerModule { }