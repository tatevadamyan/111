import { NgModule } from '@angular/core';
import { Routes, RouterModule } from "@angular/router";
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module'
import { PatientPageComponent } from './patient-page.component';

const PATIENT_ROUTES: Routes = [{
    path: "",
    component: PatientPageComponent
}];

@NgModule({
    declarations: [PatientPageComponent],
    imports: [
        SharedModule,
        FormsModule,
        RouterModule.forChild(PATIENT_ROUTES)
    ]
})
export class PatientModule { }