import { NgModule } from '@angular/core';
import { Routes, RouterModule } from "@angular/router";
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module'
import { DoctorPageComponent } from './doctor-page.component';

const DOC_ROUTES: Routes = [{
    path: "",
    component: DoctorPageComponent
}];

@NgModule({
    declarations: [DoctorPageComponent],
    imports: [
        SharedModule,
        FormsModule,
        RouterModule.forChild(DOC_ROUTES)
    ]
})
export class DoctorModule { }