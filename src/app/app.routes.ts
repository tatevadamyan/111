import { ModuleWithProviders } from '@angular/core'; 
import {Routes, RouterModule } from '@angular/router';


const ROUTES: Routes = [
    {
        path: "",
        loadChildren: "./layouts/layouts.module#LayoutsModule"
    }
];
export const AppRoutes = RouterModule.forRoot(ROUTES);
