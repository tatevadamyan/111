import { NgModule, ModuleWithProviders } from '@angular/core';
import { MdInputModule, MdCheckboxModule,
         MdCardModule, MdFormFieldModule,
         MdButtonModule, MdTabsModule, MdToolbarModule } from '@angular/material';

@NgModule({
    imports: [
        MdInputModule,
        MdCardModule,
        MdFormFieldModule,
        MdCheckboxModule,
        MdButtonModule,
        MdTabsModule,
        MdToolbarModule
    ],
    exports: [
        MdCardModule,
        MdInputModule,
        MdFormFieldModule,
        MdCheckboxModule,
        MdButtonModule,
        MdTabsModule,
        MdToolbarModule

    ]
})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule
        };
    }
}